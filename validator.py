import pandas as pd
import csv
from datetime import datetime
from Levenshtein import distance as lev
from Levenshtein import ratio


"""
In die Variable `df` werden nun alle Daten eingelesen. Wichtig ist:

- die Datei muss UTF-8 sein und Komma-separiert (nicht mit Semikolon)
"""
df = pd.read_csv('Export_alle_SuS_zum_Validieren.csv')


def durchschnittsAlterLehrer():
    """
    Das Geburtsdatum wird als 13.08.2001 angegeben

    Ausgabe ist dann:

    1979-03-19 06:09:37.540106944
    43.331550802139034


    Also 43.3 Jahre
    """
    dflehrer = pd.read_csv('Export_fuer_Sibank-Parser.csv')

    dflehrer['GEBURTSDATUM'] = pd.to_datetime(
        dflehrer['GEBURTSDATUM'], format='%d.%m.%Y')

    print(dflehrer['GEBURTSDATUM'].mean())

    now = datetime.now()

    alter = (now - dflehrer.sort_values(by='GEBURTSDATUM')
             ['GEBURTSDATUM']).astype('<m8[Y]')  # type: ignore

    print(alter.mean())


def check_telefon():
    # Filter the rows
    result = df.query(
        '(PLZ == "" and Telefonnummer == "") | (Vorwahl.str.contains(r"^[0-9]{4,5}$") & Telefonnummer.str.contains(r"^[0-9]{3,12}$"))')

    # Print the filtered DataFrame
    print(result)

def check_reli_teilnahme():
    # Reli-Teilname darf nicht leer sein
    result = df.query("`RELI. TEILNAHME AN` == '' or `RELI. TEILNAHME AN`.isna()")


    result[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE',
                        'GESCHLECHT']].to_csv("reliteilname_fehlt.csv", index=False, sep=';')

def check_wiederholer_angabefehlt():
    # Alle SuS finden, die Wiederholen aber wo der Grund dazu fehlt
    result = df[df['WIEDERHOLER'] != 'Nein']

    # ART DES WIEDERHOLES ist nicht leer, also wiederholt die Person
    ohne_grund = result[result['ART DES WIEDERHOLENS'].isna() | (result['ART DES WIEDERHOLENS'] == '')]

    # Nach Stufe und dann nach Klasse sortieren, damit man das schnell beheben kann
    ohne_grund = ohne_grund.sort_values(by=['STUFE', 'KLASSE'])

    ohne_grund[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'WIEDERHOLER', 'ART DES WIEDERHOLENS',
                        'GESCHLECHT']].to_csv("wiederholer_angabe_fehlt.csv", index=False, sep=';')


def check_herkunftssprache_angabefehlt():
    # Alle SuS finden, die keine Angabe zur Herkunftssprache haben
    result = df.query("`HERKUNFTSSPRACHE` == '' or `HERKUNFTSSPRACHE`.isna()")

    # Nach Stufe und dann nach Klasse sortieren, damit man das schnell beheben kann
    ohne_grund = result.sort_values(by=['STUFE', 'KLASSE'])

    ohne_grund[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'HERKUNFTSSPRACHE', 'GEBURTSLAND',
                        'GESCHLECHT']].to_csv("wiederholer_herkunftsspreache_fehlt.csv", index=False, sep=';')

def check_staatsang_angabefehlt():
    # Alle SuS finden, die keine Angabe zur Herkunftssprache haben
    result = df.query("`STAATSANGEHÖRIGKEIT` == '' or `STAATSANGEHÖRIGKEIT`.isna()")

    value_counts = df['STAATSANGEHÖRIGKEIT'].value_counts()
    # Konvertiere die Zählungen in einen DataFrame und sortiere nach Häufigkeit
    sorted_counts = value_counts.reset_index().rename(columns={'index': 'STAATSANGEHÖRIGKEIT', 'STAATSANGEHÖRIGKEIT': 'Häufigkeit'})

    # Anzeigen des Ergebnisses
    print(sorted_counts)

    # Nach Stufe und dann nach Klasse sortieren, damit man das schnell beheben kann
    ohne_grund = result.sort_values(by=['STUFE', 'KLASSE'])

    ohne_grund[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'STAATSANGEHÖRIGKEIT', 'GEBURTSLAND',
                        'GESCHLECHT']].to_csv("wiederholer_staatsangehoerigkeit_fehlt.csv", index=False, sep=';')

# Funktion zur Überprüfung und Ausgabe der Geburtstagsverteilung in jeder Klasse
def check_birthdays(group):
    birthday_counts = group['GEBURTSDATUM'].value_counts()
    multiple_birthdays = birthday_counts[birthday_counts > 1]

    print("=====================")
    print(multiple_birthdays)
    if not multiple_birthdays.empty:
        return multiple_birthdays
    else:
        return None

# Funktion zum Formatieren des Datums
def format_date(date):
    return date.strftime('%d.%m.%Y')

def geburtstags_paradoxon():
    # Erstelle eine leere Liste für die Ergebnisse
    results = []
    # Finde alle Klassen
    klassen = df['KLASSE'].unique()

    # Zähler für Klassen mit und ohne doppelte Geburtstagsdaten
    classes_with_duplicates = 0
    classes_without_duplicates = 0
    total_classes = len(klassen)

    # Durchlaufe jede Klasse
    for klasse in klassen:
        # Filtere die Daten für die aktuelle Klasse
        class_data = df[df['KLASSE'] == klasse]

        # Zähle die Geburtstagsdaten und filtere nur die, die mehr als einmal vorkommen
        birthday_counts = class_data['GEBURTSDATUM'].value_counts()
        multiple_birthdays = birthday_counts[birthday_counts > 1]

        # Überprüfe, ob es doppelte Geburtstagsdaten gibt
        if not multiple_birthdays.empty:
            classes_with_duplicates += 1
        else:
            classes_without_duplicates += 1

        # Falls es Geburtstagsdaten gibt, die mehr als einmal vorkommen, zeige sie an
        if not multiple_birthdays.empty:
            print(f"In der Klasse {klasse} haben folgende Personen am gleichen Tag Geburtstag:")
            for geburtstag, count in multiple_birthdays.items():
                # Formatiere das Datum zu TT.MM.JJJJ
                formatted_date = format_date(pd.to_datetime(geburtstag))
                print(f"{formatted_date} ({count})")
            print()  # Leere Zeile für bessere Lesbarkeit

        # Berechne die Prozentsätze
        percent_with_duplicates = (classes_with_duplicates / total_classes) * 100
        percent_without_duplicates = (classes_without_duplicates / total_classes) * 100

    # Zeige die Ergebnisse an
    print(f"Anzahl der Klassen mit mindestens einem doppelten Geburtstag: {classes_with_duplicates} ({percent_with_duplicates:.2f}%)")
    print(f"Anzahl der Klassen ohne doppelte Geburtstagsdaten: {classes_without_duplicates} ({percent_without_duplicates:.2f}%)")


def check_bildungsgang():
    # Der Bildungsgang muss immer ein bestimmtes Kriterium erfüllen.
    # In der Sek 2 z.B. 11A bis 11F oder 12 oder 13
    # In der Hauptschule aber 05A1 und nie 05A2 oder A3
    sek2 = df.query(
        'BILDUNGSGANG == "KGS - Gymnasium SEK II -" & ~ KLASSE.str.contains(r"^(11[A-F]|12|13)$")')

    hz = df.query(
        'BILDUNGSGANG == "KGS - Hauptschule -" & ~ KLASSE.str.contains(r"^.*[A-F]1$")')
    rz = df.query(
        'BILDUNGSGANG == "KGS - Realschule -" & ~ KLASSE.str.contains(r"^.*[A-F]2$")')
    gz = df.query(
        'BILDUNGSGANG == "KGS - Gymnasium SEK I -" & ~ KLASSE.str.contains(r"^.*[A-F]3$")')

    # Alle vier Fehlerquellen in die Datei schreiben
    pd.concat([sek2, gz, rz, hz])[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME',
                                   'BILDUNGSGANG', 'KLASSE']].to_csv("bildungsgang_falsch.csv", index=False, sep=';')


def check_geschlecht():
    geschlechterfehler = df.query(
        '~ GESCHLECHT.str.contains("M|W|D", na=False)')
    geschlechterfehler[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE',
                        'GESCHLECHT']].to_csv("geschlecht_falsch.csv", index=False, sep=';')


def check_strasse():
    strassenfehler = df.query('~ STRAßE.str.contains(r"^(?!.*strasse).*$")')
    strasseohneleerzeichen = df.query('~ STRAßE.str.contains(r" \d")')
    pd.concat([strassenfehler, strasseohneleerzeichen])[['IDENTNUMMER', 'FAMILIENNAME',
                                                         'RUFNAME', 'KLASSE', 'STRAßE']].to_csv("strasse_falsch.csv", index=False, sep=';')


def check_adelstitel():
    mask = df['EZ2 NAME'].notna() & df['EZ2 NAME'].str.startswith("zur") | df['EZ NAME'].notna() & df['EZ NAME'].str.startswith("zur") | df['EZ2 NAME'].notna() & df['EZ2 NAME'].str.startswith(
        "ter") | df['EZ NAME'].notna() & df['EZ NAME'].str.startswith("ter") | df['EZ2 NAME'].notna() & df['EZ2 NAME'].str.startswith("de") | df['EZ NAME'].notna() & df['EZ NAME'].str.startswith("de")

    matching_rows = df[mask]
    matching_rows[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE',
                   'EZ NAME', 'EZ2 NAME']].to_csv("adel.csv", index=False, sep=';')


def check_vorstorben():
    mask = df.astype(str).apply(lambda x: x.str.contains(
        "verstorben", na=False)).any(axis=1)
    matching_rows = df[mask]
    matching_rows[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE',
                   'EZ NAME', 'EZ2 NAME']].to_csv("verstorben.csv", index=False, sep=';')


def ReliWeno():
    """
    Zwischen Jahrgang 5 und 10 muss im Gymnasium und der Realschule Reli ODER Weno angegeben sein
    """
    condition = (pd.isnull(df['RELI. TEILNAHME AN'])) & ~ (df['BILDUNGSGANG'] == 'KGS - Hauptschule -') \
        & ~ df['KLASSE'].str.contains(r'^(11|12|13)')

    # Use the Boolean mask to filter the DataFrame
    df_filtered = df[condition]

    df_filtered = df_filtered.sort_values(by='KLASSE')

    # Display the filtered DataFrame
    df_filtered[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'SPRACHE2_KÜRZEL',
                 'KLASSE']].to_csv("reliweno_fehlt.csv", index=False, sep=';')


def RelianwahlenProJahrgang():
    """
    Wie wird Reli und Weno angewählt, getrennt nach Bildungsgang
    """
    counts = df.groupby(['RELI. TEILNAHME AN', 'BILDUNGSGANG']
                        ).size().reset_index(name='counts')

    counts.to_csv("relianwahlen.csv", index=False, sep=';')

    print(counts)


def zweiteFremdspracheEingegeben():
    """
    Zwischen Jahrgang 6 und 10 muss im Gymnasium einen 2. Fremdsprache eingegeben sein
    """
    condition = (pd.isnull(df['SPRACHE2_KÜRZEL'])) & ~ ((df['BILDUNGSGANG'] == 'KGS - Hauptschule -')
                                                        | (df['BILDUNGSGANG'] == 'KGS - Realschule -')) \
        & ~ df['KLASSE'].str.contains(r'^(05|11|12|13)')

    # Use the Boolean mask to filter the DataFrame
    df_filtered = df[condition]

    df_filtered = df_filtered.sort_values(by='KLASSE')

    # Display the filtered DataFrame
    df_filtered[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'SPRACHE2_KÜRZEL',
                 'KLASSE']].to_csv("zweite_sprache_fehlt.csv", index=False, sep=';')


def telefon_ez_testen():
    """
    Diese Methode testet, ob es Elterntelefonnummern gibt, die keine Vorwahl aber eine Durchwahl haben.

    also nicht 0441 und 12345678 sondern eine leere Vorwahl und dann z.B. 0441-12345678 als Durchwahl.
    """
    condition = (pd.isnull(df['EZ VORWAHL'])) & (pd.notnull(df['EZ TELEFON'])) \
        | (pd.isnull(df['EZ2 VORWAHL'])) & (pd.notnull(df['EZ2 TELEFON'])) \
        | (pd.isnull(df['EZ VORWAHL MOBIL'])) & (pd.notnull(df['EZ TELEFON MOBIL'])) \
        | (pd.isnull(df['EZ2 VORWAHL MOBIL'])) & (pd.notnull(df['EZ2 TELEFON MOBIL']))

    # Use the Boolean mask to filter the DataFrame
    df_filtered = df[condition]

    df_filtered[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'EZ VORWAHL', 'EZ TELEFON', 'EZ2 VORWAHL', 'EZ2 TELEFON',
                 'EZ VORWAHL MOBIL', 'EZ TELEFON MOBIL', 'EZ2 VORWAHL MOBIL', 'EZ2 TELEFON MOBIL']].to_csv("ez_telefon.csv", index=False, sep=';')


def keinezahl_telefonnummer():
    """
    Telefonnummern dürfen nur reine Zahlen enthalten
    """
    condition = (
        df['EZ TELEFON'].str.contains(
            r'\D') | df['EZ VORWAHL'].str.contains(r'\D')
        | df['EZ VORWAHL MOBIL'].str.contains(r'\D') | df['EZ TELEFON MOBIL'].str.contains(r'\D')
        | df['EZ2 VORWAHL MOBIL'].str.contains(r'\D') | df['EZ2 TELEFON MOBIL'].str.contains(r'\D')
        | df['EZ2 TELEFON'].str.contains(r'\D') | df['EZ2 VORWAHL'].str.contains(r'\D')
    )

    # Use the Boolean mask to filter the DataFrame
    df_filtered = df[condition]

    df_filtered = df_filtered.sort_values(by='KLASSE')

    # Display the filtered DataFrame
    df_filtered[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'EZ VORWAHL/TELEFON', 'EZ2 VORWAHL/TELEFON', 'EZ TELEFON MOBIL',
                 'EZ VORWAHL MOBIL', 'EZ2 VORWAHL MOBIL', 'EZ2 TELEFON MOBIL']].to_csv("nichtzahl_in_telefon.csv", index=False, sep=';')


def emailEZ():
    """
    gültige Emailadressen validieren ist erstaunlich komplex. Dieser reguläre Ausdruck stammt aus

    https://www.heise.de/ratgeber/Regex-fuer-Anfaenger-Regulaere-Ausdruecke-in-Python-umsetzen-7461629.html?seite=all

    ...und deckt noch nicht einmal alle Fälle ab... Aber 99%, immerhin.
    """
    valid_emails = df.query(
        '`EZ E-MAIL`.notnull() & ~ `EZ E-MAIL`.str.contains(r"^[\w.+-]+@[a-zA-Z\d-]+\.[a-zA-Z.]+$", na=False)')  # type: ignore

    valid_emails[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE',
                  'EZ E-MAIL', 'EZ2 E-MAIL']].to_csv("ez_email.csv", index=False, sep=';')


def emailOhneVorname():
    """
    Teste, ob es Erziehungsberechtigte mit EMail aber ohne Vorname oder Nachname gibt
    """
    fehlerzeilen = df.query(' \
          `EZ E-MAIL`.notnull() & ~ `EZ NAME`.notnull()  \
        | `EZ2 E-MAIL`.notnull() & ~ `EZ2 NAME`.notnull()  \
        | `EZ E-MAIL`.notnull() & ~ `EZ VORNAME`.notnull()  \
        | `EZ2 E-MAIL`.notnull() & ~ `EZ2 VORNAME`.notnull() ')

    fehlerzeilen[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'EZ E-MAIL', 'EZ VORNAME', 'EZ NAME',
                  'EZ2 E-MAIL', 'EZ2 VORNAME', 'EZ2 NAME']].to_csv("ez_email_namefalsch.csv", index=False, sep=';')


def telefonOhneVorname():
    """
    Teste, ob es Erziehungsberechtigte mit Telefonnummer aber ohne Vorname oder Nachname gibt
    """
    fehlerzeilen = df.query(' \
          `EZ TELEFON`.notnull() & ~ `EZ NAME`.notnull()  \
        | `EZ2 TELEFON`.notnull() & ~ `EZ2 NAME`.notnull()  \
        | `EZ TELEFON`.notnull() & ~ `EZ VORNAME`.notnull()  \
        | `EZ2 TELEFON`.notnull() & ~ `EZ2 VORNAME`.notnull() ')

    fehlerzeilen[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'KLASSE', 'EZ VORWAHL/TELEFON', 'EZ VORNAME', 'EZ NAME',
                  'EZ2 VORWAHL/TELEFON', 'EZ2 VORNAME', 'EZ2 NAME']].to_csv("ez_telefon_namefalsch.csv", index=False, sep=';')


def check_age():

    df['GEBURTSDATUM'] = pd.to_datetime(df['GEBURTSDATUM'], format='%d.%m.%y')

    # Calculate the age of each person
    today = datetime.today()
    df['age'] = (today - df['GEBURTSDATUM']).dt.total_seconds() / \
        (365.25 * 24 * 3600)

    df_filtered = df[(df['age'] < 10) | (df['age'] > 20)]

    df_filtered[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME',
                 'KLASSE', 'age']].to_csv('age.csv', index=False)


def check_plz():

    df['EZ PLZ'] = df['EZ PLZ'].astype(str)
    df['EZ2 PLZ'] = df['EZ2 PLZ'].astype(str)
    df['PLZ'] = df['PLZ'].astype(str)

    maskez1 = ~(df['EZ PLZ'].str.match(r'^\d{5}$|^$'))
    maskez2 = ~(df['EZ2 PLZ'].str.match(r'^\d{5}$|^$'))
    masks = ~(df['PLZ'].str.match(r'^\d{5}$|^$'))

    df_filteredez1 = df.loc[maskez1]
    df_filteredez2 = df.loc[maskez2]
    df_filtereds = df.loc[masks]

    # Display the filtered DataFrame
    df_filteredez1[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'EZ PLZ',
                    'KLASSE']].to_csv("postleitzahlenez1.csv", index=False, sep=';')
    df_filteredez2[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'EZ2 PLZ',
                    'KLASSE']].to_csv("postleitzahlenez2.csv", index=False, sep=';')
    df_filtereds[['IDENTNUMMER', 'FAMILIENNAME', 'RUFNAME', 'PLZ',
                  'KLASSE']].to_csv("postleitzahlens.csv", index=False, sep=';')


def levenshtein_berechnen(name1, name2):
    """
    Berechne die Levenshtein-Distanz, um Tippfehler zu finden

    https://de.wikipedia.org/wiki/Levenshtein-Distanz
    """
    if not name1 or not name2 or pd.isna(name1) or pd.isna(name2):
        return -1  # Return -1 if one or both parameters are empty or contain NaN

    return lev(name1, name2)


def is_value_in_range(value):
    """
    für die Levenshtein-Distanz wählen wie einen Distanz von 1-3.

    0 bedeutet: Komplett gleich, also kein Fehler
    1-3 ist in der Regel ein Fehler ("Strasse vs. Straße" ist zum Beispiel eine Distanz von 2)
    ab 4 ist es auch öfters korrekt.

    Man könnte auch 1-4 oder 1-5 testen
    """
    return 1 <= value <= 3


def elternadressen_aehnlichkeit():
    """
    Es gibt ja das Problem, dass IServ die Kind-Elternbeziehung (auch) anhand der Adressen erkennt. In der Realität ist es aber so, dass in den Verwaltungsdatenbanken Tippfehler exisitieren.

    Beispiele:

        Lange Str. 1 vs Lange Straße 1 vs Lange Strasse 1. vs Langestr 1. vs Langestr. 1

    Das ist alles das gleiche aber für die Software halt 5 Strings. IServ versucht bereits, Ähnlichkeiten zu bewerten, damit die 5 o.g. Straßen eben als gleich angesehen werden. Viel besser wäre es natürlich, wenn die Fehler gar nicht erst existieren würden.

    Ich komme gerade von einer Schulleitertagung wo es genau um sowas ging. Ich habe mich an die Levenshtein-Distanz erinnert, wie man "Ähnlich aber nicht gleich, also Tippfehler" mit finden kann. Der unten stehende Code findet im Grunde alle Tippfehler zwischen

    Kind - Eltern1
    Kind - Eltern2
    Eltern1- Eltern1
    """

    fehler_kind_ez1 = []
    fehler_kind_ez2 = []
    fehler_ez1_ez2 = []

    for index, row in df.iterrows():
        # jeden Schüler durchgehen
        kind_strasse = row['STRAßE']
        EZ_strasse = row['EZ STRAßE']
        EZ2_strasse = row['EZ2 STRAßE']

        # Für die drei möglichen Beziehungen die Distanz berechnen
        levenshtein_kind_ez1 = levenshtein_berechnen(kind_strasse, EZ_strasse)
        levenshtein_kind_ez2 = levenshtein_berechnen(kind_strasse, EZ2_strasse)
        levenshtein_ez1_ez2 = levenshtein_berechnen(EZ_strasse, EZ2_strasse)

        fehler = []

        if is_value_in_range(levenshtein_kind_ez1):
            fehler.append(row['KLASSE'])
            fehler.append(row['RUFNAME'])
            fehler.append(row['FAMILIENNAME'])
            fehler.append(kind_strasse)
            fehler.append(EZ_strasse)
            fehler.append(levenshtein_kind_ez1)

            fehler_kind_ez1.append(fehler)

        fehler = []

        if is_value_in_range(levenshtein_kind_ez2):
            fehler.append(row['KLASSE'])
            fehler.append(row['RUFNAME'])
            fehler.append(row['FAMILIENNAME'])
            fehler.append(kind_strasse)
            fehler.append(EZ2_strasse)
            fehler.append(levenshtein_kind_ez2)

            fehler_kind_ez2.append(fehler)

        fehler = []

        if is_value_in_range(levenshtein_ez1_ez2):
            fehler.append(row['KLASSE'])
            fehler.append(row['RUFNAME'])
            fehler.append(row['FAMILIENNAME'])
            fehler.append(EZ_strasse)
            fehler.append(EZ2_strasse)
            fehler.append(levenshtein_ez1_ez2)

            fehler_ez1_ez2.append(fehler)

    # Die Listen nach Klasse sortieren
    fehler_ez1_ez2_sorted = sorted(fehler_ez1_ez2, key=lambda x: x[0])
    fehler_kind_ez1_sorted = sorted(fehler_kind_ez1, key=lambda x: x[0])
    fehler_kind_ez2_sorted = sorted(fehler_kind_ez2, key=lambda x: x[0])

    # Die drei Listen als .csv-Datei speichern
    with open("fehler_kind_ez1.csv", 'a', newline='') as file:
        csv_writer = csv.writer(file)

        for row in fehler_kind_ez1_sorted:
            csv_writer.writerow(row)

    with open("fehler_kind_ez2.csv", 'a', newline='') as file:
        csv_writer = csv.writer(file)

        for row in fehler_kind_ez2_sorted:
            csv_writer.writerow(row)

    with open("fehler_ez1_ez2.csv", 'a', newline='') as file:
        csv_writer = csv.writer(file)

        for row in fehler_ez1_ez2_sorted:
            csv_writer.writerow(row)


def liste_aller_plz():
    """
    Listet alle Postleitzahlen auf, die nicht 26180 und 26215 (Wiefelstede) sind
    """
    df['PLZ'] = df['PLZ'].astype(str)
    masks = ~(df['PLZ'].str.match(r'^(26180|26215)$'))
    df_filtereds = df.loc[masks]

    # Display the filtered DataFrame
    df_filtereds[
        ['IDENTNUMMER', 'PLZ', 'ORT', 'FAMILIENNAME', 'RUFNAME', 'KLASSE']].sort_values(by="PLZ").to_csv("postleitzahlenlistey.csv", index=False, sep=';')


if __name__ == '__main__':
    check_geschlecht()
    check_strasse()

    # Für diesen Check braucht man noch Lehrerdaten, ist eher eine Spielerrei
    # durchschnittsAlterLehrer()

    # Die folgenden drei Methoden setzen den Bildunggang an KGSen voraus.
    # An IGSen und anderen Schulformen müssen sie auskommentiert werden
    check_bildungsgang()                # Nur für KGSen
    zweiteFremdspracheEingegeben()      # Nur für KGSen
    ReliWeno()                          # Nur für KGSen

    RelianwahlenProJahrgang()

    # Hier liegt noch ein Fehler vor, der Code funktioniert nicht
    # keinezahl_telefonnummer()

    emailEZ()
    telefon_ez_testen()
    telefonOhneVorname()
    emailOhneVorname()
    check_adelstitel()
    check_vorstorben()
    check_age()
    check_plz()
    liste_aller_plz()
    elternadressen_aehnlichkeit()
    check_reli_teilnahme()
    check_wiederholer_angabefehlt()
    check_herkunftssprache_angabefehlt()
    check_staatsang_angabefehlt()
    geburtstags_paradoxon()
